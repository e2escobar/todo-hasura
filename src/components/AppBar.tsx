import React from 'react'
import { PageHeader, Typography, Button } from 'antd'
import { createUseStyles } from 'react-jss'
import { Link } from "@reach/router"

const { Text } = Typography

const useStyles = createUseStyles({
  AppBar: {
      background: 'rgba(237, 237, 237, 0.58)'
  },
  AppTitle: {
    fontWeight: 300,
  }
})

interface AppBarProps {}

export const AppBar = ({}: AppBarProps) => {
  
  const classes = useStyles()

  return (
    <PageHeader
      className={classes.AppBar}
      title={<Text className={classes.AppTitle} type="secondary">Another To Do App</Text>}
      extra={[
        <Link key="1" to="/login">
          <Button  type="primary">
            Login
          </Button> 
        </Link>
      ]}
    />
  )
}