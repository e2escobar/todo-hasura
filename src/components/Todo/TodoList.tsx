import React, { useEffect, useState } from 'react'
import { List } from 'antd'
import { useQuery } from '@apollo/client'
import { GET_TASKS } from '../../GraphQL/Queries'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  TaskList: {
    backgroundColor: '#FFFFFF',
    maxHeight: '300px',
    overflow: 'auto'
  }
})

import { TodoItem } from './TodoItem'
import { TodoInput } from './TodoInput'

export const TodoList = () => {

  const classes = useStyles()
  const {error, loading, data, refetch } = useQuery(GET_TASKS)

  const [tasks, setTasks] = useState([])

  useEffect(() => {
    console.log("Entering use effect");
    if(data) {
      setTasks(data.task);
    } 
  }, [data])


  return (
    <>
      <TodoInput refetch={() => refetch()} />
      <List
        className={classes.TaskList}
        itemLayout="horizontal"
        dataSource={tasks}
        bordered
        renderItem={item => (
          <TodoItem item={item} />
        )}
      />
    </>

  )
}
