import React from 'react'
import { CheckCircleFilled } from '@ant-design/icons'
import { createUseStyles } from 'react-jss'
import  Circle  from '../../Circle.svgr.svg'

interface TodoIconProps {
  done?: Boolean
}

const useStyles = createUseStyles({
  TodoIcon: {
    '&:hover': { 
      color: "rgb(232,146,82)"
    },
    '&.done': {
      color: "rgb(232,146,82)"
    },
    fontSize: '20px',
    cursor: 'pointer',
  }
})

export const TodoIcon = ({ done } : TodoIconProps) => {

  const classes = useStyles();
  
  const icon = done ? <CheckCircleFilled /> : <Circle />
  const isDone = done ? 'done' : 'todo'

  return (
    <i className={`${classes.TodoIcon} ${isDone}`}>
      {icon}
    </i>
  )
}