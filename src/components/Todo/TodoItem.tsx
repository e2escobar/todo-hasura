import React from 'react'
import { List } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { createUseStyles } from 'react-jss'
import { TodoIcon } from './TodoIcon'

interface TodoItemProps {
  item: Task,
}

interface Task {
  created_at: String
  id: String
  is_done: Boolean
  text: String
}


const useStyles = createUseStyles({
  ActionIcon: {
    fontSize: '20px',
    cursor: 'pointer',    
  },
  DeleteIcon: { 
    extend: 'ActionIcon',
    color: "#e25a2d"
  },
  DoneTask: {
    textDecoration: 'line-through'
  }
})

export const TodoItem = ({ item }: TodoItemProps) => {

  const classes = useStyles();

  return (
    <List.Item
    className={item.is_done && classes.DoneTask}
    actions={
      [
        <i className={classes.ActionIcon} key="list-loadmore-edit"><EditOutlined /></i>,
        <i className={classes.DeleteIcon} key="list-loadmore-delete"><DeleteOutlined /></i>
      ]
    }
    >
      <List.Item.Meta
        style={{ alignItems: 'center' }}
        avatar={<TodoIcon done={item.is_done}/>}
        title={<span>{item.text}</span>}
      />
    </List.Item>
  )
}
