import React, { KeyboardEvent, useState } from 'react'
import { Input, Button } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';
import { createUseStyles } from 'react-jss'
import { useMutation } from '@apollo/client'
import { ADD_NEW_TASK } from '../../GraphQL/Mutations'

const useStyles = createUseStyles({
  TaskInput: {
    marginBottom: '20px',
    '.ant-input-group-addon': {
      backgroundColor: 'red'
    }
  },
  AddIcon: {
    fontSize: '20px',
    color: "rgb(232,146,82)"
  }
})

export const TodoInput = ({ refetch }) => {

  const [task, setTask] = useState('')

  const [AddNewTask, { error }] = useMutation(ADD_NEW_TASK)
  
  const addNewTask = () => {
    if(task) {
      AddNewTask({ 
        variables: {
          text: task
        },
      }).then(() => {
        console.log('refetch');
        // refetch() 
      })
      setTask('')
      if(error) {
        console.error(error)
      }
    }
  }
  
  const classes = useStyles()

  return (
    <Input 
      className={classes.TaskInput} 
      size="large" 
      placeholder="Add New Task"
      value={task}
      onChange={(e) => setTask(e.target.value)}
      onKeyDown={e => e.key === 'Enter' && addNewTask()} />
  )
}
