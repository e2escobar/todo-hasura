import {gql} from '@apollo/client'

export const GET_TASKS = gql`
  query GetTaskList {
    task (order_by: {is_done: asc}) {
      created_at
      id
      is_done
      text
    }
  }
`

export const GET_DONE_TASKS = gql`
  query GetTaskList {
    task (where: {is_done: {_eq: true}}){
      created_at
      id
      is_done
      text
    }
  }
`