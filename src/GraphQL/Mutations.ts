import {gql} from '@apollo/client'

export const ADD_NEW_TASK = gql`
  mutation AddNewTask($text: String!) {
    insert_task(objects: { text: $text }) {
      returning {
        created_at
        id
        is_done
        text
      }
    }
  }
`