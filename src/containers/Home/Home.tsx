import React from 'react'
import { Typography, Layout } from 'antd';

import { TodoList } from '../../components/Todo/TodoList'

const { Title, Paragraph, Text, Link } = Typography;


export const Home = () =>  {
  return (
    <Layout> 
      <Typography style={{textAlign: 'center', marginBottom: '20px'}}>
        <Title level={3}>Tasks List </Title>
      </Typography>
      <TodoList />
    </Layout>
  )
  
}
