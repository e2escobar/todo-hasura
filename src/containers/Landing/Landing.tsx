import React from 'react'
import { Typography, Layout } from 'antd';

const { Title, Paragraph, Text, Link } = Typography;

export const Landing = () =>  {
  return (
    <Layout> 
      <Typography>
        <Title>Hi! 👋 </Title>
        <Paragraph>
          This is another To-Do App using React, Hasura and Apollo Client.
          The main goal is to show how to connect Hasura and apollo client in your applications
          and also how to use Hasura Actions to manage the authentication process. 
        </Paragraph>

        <Title level={2}>Credentials: </Title>
        <Paragraph>
          Pls use this credentials to login for testing purposes. <br/>
          <Text strong>email: </Text> test@anothertodoapp.com <br/>
          <Text strong>passwd: </Text> supersecure 

        </Paragraph>
        <Title level={2}>Find me on Github</Title>
        <Paragraph>
          Feel fre to clone, modify and distribute this code.
        </Paragraph>
      </Typography>
    </Layout>
  )
  
}
