import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from
} from '@apollo/client'
import {onError} from '@apollo/client/link/error'


// Erro handler for GraphQL
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if(graphQLErrors) {
    graphQLErrors.map(({message, locations, path}) => {
      console.log(`GraphQL Error: ${message}`);
    })
  }
})

// Link for connection
const link = from([
  errorLink,
  new HttpLink({uri: 'http://localhost:8080/v1/graphql'})
])

// Client declaration 
const client = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: true
  }),
  link
})
