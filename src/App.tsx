import React, { useState, useEffect } from 'react'
import { Router, Link, RouteComponentProps } from "@reach/router"
import { Layout, message } from 'antd'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from
} from '@apollo/client'
import {onError} from '@apollo/client/link/error'

import { AppBar } from './components/AppBar'

import { Landing } from './containers/Landing/Landing'
import { Login } from './containers/Login/Login'
import { Home } from './containers/Home/Home'

import { createUseStyles } from 'react-jss'

import './App.css';

const { Footer, Content } = Layout;


const useStyles = createUseStyles({
  Layout: {
    maxWidth: '800px',
    margin: '40px auto'
  },
  Content: {
    padding: '20px'
  }
})

// Erro handler for GraphQL
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if(graphQLErrors) {
    graphQLErrors.map(({message, locations, path}) => {
      console.log(`GraphQL Error: ${message}`);
    })
  }
})

// Link for connection
const link = from([
  errorLink,
  new HttpLink({uri: 'http://localhost:8080/v1/graphql'})
])

// Client declaration 
const client = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: true
  }),
  link
})


interface AppProps {}

function App({}: AppProps) {
  // Return the App component.
  const classes = useStyles()
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <AppBar />
        <Layout className={classes.Layout}>
          <Content className={classes.Content}>
            <Router>
              <RouterPage pageComponent={<Landing />} path="/" default={true} />
              <RouterPage pageComponent={<Home />} path="/home" />
              <RouterPage pageComponent={<Login />} path="/login" />
            </Router>
          </Content>
          <Footer>
          <nav>
            <ul>
              <li>
                <Link to="/">landing</Link>
              </li>
              <li>
                <Link to="/home">home</Link>
              </li>
              <li>
                <Link to="/login">Login</Link>
              </li>
            </ul>
          </nav>
          </Footer>
        </Layout>
      </div>
    </ApolloProvider>
  );
}

export default App;

const RouterPage = (
  props: { pageComponent: JSX.Element } & RouteComponentProps
) => props.pageComponent;
