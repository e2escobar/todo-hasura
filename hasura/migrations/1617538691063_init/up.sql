CREATE TABLE public.task (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    text text NOT NULL,
    is_done boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE public."user" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    email text NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    role text DEFAULT 'admin'::text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);
ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT users_email_key UNIQUE (email);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT users_pkey1 PRIMARY KEY (id);
ALTER TABLE ONLY public."user"
    ADD CONSTRAINT users_username_key UNIQUE (username);
